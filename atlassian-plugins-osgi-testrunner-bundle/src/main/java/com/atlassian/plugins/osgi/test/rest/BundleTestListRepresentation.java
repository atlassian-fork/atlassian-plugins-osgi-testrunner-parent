package com.atlassian.plugins.osgi.test.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atlassian.plugins.osgi.test.util.TestClassUtils;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @since version
 */
public class BundleTestListRepresentation
{
    @JsonProperty private final String bundleSymbolicName;
    @JsonProperty private final Map<TestClassUtils.TestType,List<TestDetailRepresentation>> testMap;

    @JsonCreator
    public BundleTestListRepresentation(@JsonProperty("bundleSymbolicName") String bundleSymbolicName)
    {
        this.bundleSymbolicName = bundleSymbolicName;
        this.testMap = new HashMap<TestClassUtils.TestType, List<TestDetailRepresentation>>(3);
        testMap.put(TestClassUtils.TestType.Wired,new ArrayList<TestDetailRepresentation>());
        testMap.put(TestClassUtils.TestType.Unit,new ArrayList<TestDetailRepresentation>());
        testMap.put(TestClassUtils.TestType.IT,new ArrayList<TestDetailRepresentation>());
    }

    public String getBundleSymbolicName()
    {
        return bundleSymbolicName;
    }

    public Map<TestClassUtils.TestType, List<TestDetailRepresentation>> getTestMap()
    {
        return testMap;
    }
    
    public List<TestDetailRepresentation> getUnitTests()
    {
        return testMap.get(TestClassUtils.TestType.Unit);
    }

    public List<TestDetailRepresentation> getITTests()
    {
        return testMap.get(TestClassUtils.TestType.IT);
    }

    public List<TestDetailRepresentation> getWiredTests()
    {
        return testMap.get(TestClassUtils.TestType.Wired);
    }
    
}
