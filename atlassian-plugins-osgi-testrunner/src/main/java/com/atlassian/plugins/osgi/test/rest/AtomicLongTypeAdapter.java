package com.atlassian.plugins.osgi.test.rest;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.concurrent.atomic.AtomicLong;

public class AtomicLongTypeAdapter implements JsonSerializer<AtomicLong>, JsonDeserializer<AtomicLong> {

    @Override
    public AtomicLong deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        return new AtomicLong(json.getAsLong());
    }

    @Override
    public JsonElement serialize(AtomicLong src, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive(src.get());
    }
}
