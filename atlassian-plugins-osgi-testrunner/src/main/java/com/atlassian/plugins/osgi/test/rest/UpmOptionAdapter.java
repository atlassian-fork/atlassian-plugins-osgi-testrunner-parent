package com.atlassian.plugins.osgi.test.rest;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import com.atlassian.upm.api.util.Option;

import com.google.gson.*;

public class UpmOptionAdapter implements JsonSerializer<Option>, JsonDeserializer<Option>
{

    @Override
    public Option deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
    {
        Gson gson = new Gson();

        if(typeOfT instanceof ParameterizedType)
        {
            Type nestedType = ((ParameterizedType) typeOfT).getActualTypeArguments()[0];
            return Option.some(gson.fromJson(json,nestedType));
        }
        
        return Option.none();
    }

    @Override
    public JsonElement serialize(Option src, Type typeOfSrc, JsonSerializationContext context)
    {
        if(typeOfSrc instanceof ParameterizedType)
        {
            Type nestedType = ((ParameterizedType) typeOfSrc).getActualTypeArguments()[0];

            Gson gson = new Gson();
            
            if(src.isDefined())
            {
                return context.serialize(gson.toJson(src.get()));
            }
        }
        return null;
    }
}
